<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Outlet;
use Faker\Generator as Faker;

$factory->define(Outlet::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'active' => rand(0, 1),
    ];
});
