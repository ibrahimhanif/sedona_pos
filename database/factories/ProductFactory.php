<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'category_id' => rand(1, 5),
        'user_id' => rand(1, 5),
        'outlet_id' => rand(1, 5),
        'name' => $faker->word,
        'qty' => $faker->randomDigit,
        'price' => rand(1000, 100000)
    ];
});
