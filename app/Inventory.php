<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $fillable = [
        'id',
        'description',
        'type'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'inventory_details')->withPivot('qty');
    }
}
