<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
    protected $fillable = [
        'name', 'address', 'phone', 'active'
    ];

    public function owner(){
        return $this->belongsToMany(User::class)->where('owner', true);
    }

    public function employee(){
        return $this->belongsToMany(User::class)->where('owner', false);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }
}
