<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username','email','password','api_token','owner'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function outlet(){
        if($this->owner){
            return $this->belongsToMany(Outlet::class);
        }else{
            return $this->belongsToMany(Outlet::class)->first();
        }
    }

    public function products(){
        return $this->hasMany(Product::class);
    }
}
