<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $header_table = [
            'id' => 'ID',
            'name' => 'Nama user',
            'username' => 'Username',
            'email' => 'Email',
            'action' => 'Action'
        ];
        if (request()->ajax()) {
            $users = User::all(['id', 'name', 'username', 'email']);
            return DataTables::of($users)->addColumn('action', function ($data) {
                $edit = '<button onclick="edit_data(' . $data->id . ')" class="btn btn-sm btn-primary"><i class="flaticon flaticon-pencil"></i> Edit</button>';
                $delete = '<button onclick="delete_data(' . $data->id . ')" class="btn btn-sm btn-danger"><i class="flaticon flaticon-close"></i> Delete</button>';
                $ahir = (Auth::user()->id == $data->id) ? $edit : $edit . $delete;
                $action = '<div class="btn-group" role="group" aria-label="Basic example">' . $ahir . '</div>';
                return $action;
            })->rawColumns(['action'])->make(true);
        }
        return view('user.index', compact('header_table'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required',
            'username' => 'required',
            'email' => 'required',
            'photo' => 'required'
        ]);
        $input = $request->all();
        $image = $request->file('photo');
        $input['type'] = 0;
        $name = 'image-' . rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('img'), $name);
        $input['photo'] = $name;
        $input['password'] = Hash::make($request->password);
        $input['api_token'] = Str::random(80);
        User::create($input);
        return response()->json('Success');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user = $user->only([
            'name', 'email', 'password',
            'username', 'photo'
        ]);
        return response()->json($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $input = $request->all();
        $user->update($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
    }
}
