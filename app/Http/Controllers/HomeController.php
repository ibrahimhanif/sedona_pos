<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Category;
use App\Product;
use App\Outlet;

class HomeController extends Controller
{
    public function  index(){
        $jumlah_data = [
            'pegawai' => User::count(),
            'category' => Category::count(),
            'product' => Product::count(),
            'outlet' => Outlet::count()
        ];
        return view('home', compact('jumlah_data'));
    }
}
