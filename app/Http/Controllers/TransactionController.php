<?php

namespace App\Http\Controllers;

use App\Product;
use App\Transaction;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $header_table = [
            'id' => 'ID',
            'invoice' => 'Invoice',
            'final_price' => 'Harga',
            'cash' => 'Uang Bayar',
            'change' => 'Kembalian',
            'action' => 'Aksi'
        ];
        if (request()->ajax()) {
            $transactions = Transaction::all(['id', 'final_price', 'invoice', 'cash', 'change']);
            return DataTables::of($transactions)->editColumn('change', function ($product) {
                return "Rp. " . number_format($product->change, 0, ',', '.');
            })->editColumn('final_price', function ($product) {
                return "Rp. " . number_format($product->final_price, 0, ',', '.');
            })->editColumn('cash', function ($product) {
                return "Rp. " . number_format($product->cash, 0, ',', '.');
            })->addColumn('action', function ($data) {
                $show = '<button onclick="show_detail_transaction(' . $data->id . ')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i> Detail</button>';
                $invoice = '<button onclick="invoice(' . $data->id . ')" class="btn btn-sm btn-warning"><i class="fa fa-info"></i> Invoice</button>';
                $action = '<div class="btn-group" role="group" aria-label="Basic example">' . $show . $invoice . '</div>';
                return $action;
            })->rawColumns(['action'])->make(true);
        }
        $products = Product::all();
        return view('transaction.index', compact('header_table', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'invoice' => 'required',
            'final_price' => 'required|gte:0',
            'cash' => 'required|gte:final_price',
            'change' => 'required',
            'list_barang' => 'required'
        ]);
        $list_barang =  json_decode($request->list_barang);
        $iput_transaksi = $request->except('list_barang');
        $iput_transaksi['invoice'] = $request->invoice . Transaction::max('id');
        $iput_transaksi['user_id'] = Auth::user()->id;
        $transaction = Transaction::create($iput_transaksi);
        foreach ($list_barang as $barang) {
            $product = Product::find($barang->id);
            $product->qty = $product->qty - $barang->qty;
            $product->save();
            $transaction->products()->attach($barang->id, ['qty' => $barang->qty, 'final_price' => $barang->final_price]);
        }
        return response()->json(['message' => 'Berhasil Menambahkan Transaksi'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        $products = $transaction->load('products')->products;
        foreach ($products as $product) {
            $product->price = number_format($product->price, 0, ',', '.');
            $product->final_price = number_format($product->price, 0, ',', '.');
        }
        return $products;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    public function invoice($id)
    {
        $transaction = Transaction::find($id);
        $products = $transaction->load('products')->products;
        return view('invoice.index', compact('transaction', 'products'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
