<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\Outlet;
use App\Product;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $header_table = [
            'id' => 'ID',
            'description' => 'Deskripsi',
            'type' => 'Tipe Invenory',
            'action' => 'Aksi'
        ];
        $inventories = Inventory::all();
        if (request()->ajax()) {
            return DataTables::of($inventories)->addColumn('action', function ($data) {
                $show = '<button onclick="show_inventory_detail(' . $data->id . ')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i> Detail</button>';
                $action = '<div class="btn-group" role="group" aria-label="Basic example">' . $show . '</div>';
                return $action;
            })->rawColumns(['action'])->make(true);
        }
        $outlets = Outlet::all()->pluck('name', 'id');
        $products = Product::all();
        return view('inventory.index', compact('header_table', 'outlets', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $list_barang =  json_decode($request->list_barang);
        $input_inventory = $request->except('list_barang');
        $inventory = Inventory::create($input_inventory);
        foreach ($list_barang as $barang) {
            $product = Product::find($barang->id);
            if ($request->type == 'in') {
                $product->qty = $product->qty + $barang->qty;
            } else {
                $product->qty = $product->qty - $barang->qty;
            }
            $product->save();
            $inventory->products()->attach($barang->id, ['qty' => $barang->qty]);
        }
        return response()->json(['message' => 'Berhasil'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(Inventory $inventory)
    {
        return $inventory->load('products');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventory $inventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventory $inventory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inventory $inventory)
    {
        //
    }
}
