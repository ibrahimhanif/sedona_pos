<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'qty', 'price', 'photo', 'category_id','user_id', 'outlet_id'
    ];

    public function outlet(){
        return $this->belongsTo(Outlet::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
