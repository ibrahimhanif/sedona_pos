<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'user_id',
        'invoice',
        'note',
        'final_price',
        'cash',
        'change'
    ];
    public function products()
    {
        return $this->belongsToMany(Product::class, 'transaction_details')->withPivot('qty', 'final_price');
    }
}
