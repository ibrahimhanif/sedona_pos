@extends('layouts.app')
@section('title', 'Transaki')
@section('content')
<x-data-table title="Transaksi" :header="$header_table" resource="transactions">
    @include('transaction.form')
</x-data-table>
<div class="modal fade bd-example-modal-lg" id="modal_detail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <table class="table" style="width: 100%" id="table_detail">
                    <thead>
                        <th>Nama Barang</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                        <th>Harga Total</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <div class="modal-footer border-0">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="close_btn">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<style>
    #modal_form .modal-dialog {
        width: 100%;
        height: 100%;
        padding: 0;
        margin: 0;
    }

    #modal_form .modal-content {
        height: 100%;
        border-radius: 0;
        color: #333;
        overflow: auto;
    }

    #modal_form .close {
        color: black ! important;
        opacity: 1.0;
    }

    @media (min-width: 992px) {
        #modal_form .modal-lg {
            max-width: 100%;
        }
    }

    @media (min-width: 576px) {
        #modal_form .modal-lg {
            max-width: 100%;
        }
    }
</style>
@endpush

@push('js')
<script>
    var barang = [];
    var table_detail = $("#table_detail").dataTable({
                    data:barang,
                    theme: 'bootstrap',
                    columns:[
                        {title: 'Nama Barang', data:'name'},
                        {title: 'Jumlah', data:'qty'},
                        {title: 'Harga', data:'price'},
                        {title: 'Total', data:'final_price'},
                    ],
                });
    function invoice(id){
        let url = "{{url('invoice')}}"+'/'+id;
        window.location.href = url;
    }
    function show_detail_transaction(id){
        let url = "{{route('transactions.index')}}"+'/'+id;

        barang = []
        $.ajax({
            url: url,
            type:'GET',
            dataType: 'JSON',
            success: function(data){
                data.forEach(product => {
                    console.log(product);
                    let temp_product = new Product(product.id, product.name, product.pivot.qty,"Rp. "+ product.price, "Rp. "+ product.pivot.final_price);
                    barang.push(temp_product);
                });
                table_detail.fnClearTable();
                barang.forEach(function (val) {
                    table_detail.fnAddData(val);
                });
                $("#modal_detail").modal('show');
            }

        })
    }
</script>
@endpush
