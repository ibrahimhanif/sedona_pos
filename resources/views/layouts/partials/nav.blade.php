<div class="sidebar sidebar-style-2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="{{ asset("img/profile.jpg") }}" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            {{Auth::user()->name}}
                            <span class="user-level">@owner
                                Admin(Owner) @else Kasir
                                @endowner</span>
                        </span>
                    </a>
                </div>
            </div>
            <ul class="nav nav-primary" data-background-color="purple">
                <li class="nav-item @url('home') active @endurl">
                    <a href="{{ URL('home') }}">
                        <i class="fas fa-home"></i>
                        <p>Home</p>
                    </a>
                </li>
                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Menu Menejemen</h4>
                </li>
                @if (Auth::user()->owner)
                <li class="nav-item @url('outlets') active @endurl">
                    <a href="{{ URL('outlets') }}">
                        <i class="fas fa-store-alt"></i>
                        <p>Outlets</p>
                    </a>
                </li>
                <li class="nav-item @url('users') active @endurl">
                    <a href="{{ URL('users') }}">
                        <i class="fas fa-user-alt"></i>
                        <p>Users</p>
                    </a>
                </li>

                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Produk</h4>
                </li>
                <li class="nav-item @url('inventory/categories') active  @endurl">
                    <a href="{{ route('categories.index') }}">
                        <i class="fas fa-th-list"></i>
                        <p>Master Kategori</p>
                    </a>
                </li>

                <li class="nav-item @url('inventory/products') active  @endurl">
                    <a href="{{ route('products.index') }}">
                        <i class="fas fa-th"></i>
                        <p>Master Produk</p>
                    </a>
                </li>

                <li class="nav-item @url('inventories') active  @endurl">
                    <a href="{{ url('inventories') }}">
                        <i class="fas fa-th"></i>
                        <p>Kelola Stok</p>
                    </a>
                </li>
                @endif
                <li class="nav-item">
                    <a href="{{ URL('transactions') }}">
                        <i class="fa fa-shopping-bag"></i>
                        <p>Transaksi</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
