@extends('layouts.app')

@section('content')
<div class="page-inner">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-9">
            <div class="row align-items-center">
                <div class="col">
                    <h6 class="page-pretitle">
                        Invoice
                    </h6>
                    <h4 class="page-title">Invoice {{$transaction->invoice}}</h4>
                </div>
                <div class="col-auto">
                    <button class="btn btn-light btn-border" onclick="pdf()">
                        Print
                    </button>
                </div>
            </div>
            <div class="page-divider"></div>
            <div class="row" id="invoice">
                <div class="col-md-12">
                    <div class="card card-invoice" id="invoice">
                        <div class="card-header">
                            <div class="invoice-header">
                                <h3 class="invoice-title">
                                    Sedona
                                </h3>
                            </div>
                            <div class="invoice-desc">
                                Jl. Tanjungpura No.18, Indramayu<br>
                                Telp 0852-2000-2400
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="separator-solid"></div>
                            <div class="row">
                                <div class="col-md-4 info-invoice">
                                    <h5 class="sub">Date</h5>
                                    <p>{{$transaction->created_at}}</p>
                                </div>
                                <div class="col-md-4 info-invoice">
                                    <h5 class="sub">Invoice ID</h5>
                                    <p>{{$transaction->invoice}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="invoice-detail">
                                        <div class="invoice-top">
                                            <h3 class="title"><strong>List Produk</strong></h3>
                                        </div>
                                        <div class="invoice-item">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <td><strong>Nama</strong></td>
                                                            <td class="text-center"><strong>Harga</strong></td>
                                                            <td class="text-center"><strong>Jumlah</strong>
                                                            </td>
                                                            <td class="text-right"><strong>Total</strong></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($products as $product)
                                                        <tr>
                                                            <td>{{$product->name}}</td>
                                                            <td class="text-center">@currency($product->price)</td>
                                                            <td class="text-center">{{$product->pivot->qty}}</td>
                                                            <td class="text-right">
                                                                @currency($product->pivot->final_price)
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="separator-solid  mb-3"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-sm-7 col-md-5 mb-3 mb-md-0 transfer-to">
                                </div>
                                <div class="col-sm-5 col-md-7 transfer-total">
                                    <h5 class="sub">Total Harga</h5>
                                    <div class="price">@currency($transaction->final_price)</div>
                                </div>
                            </div>
                            <div class="separator-solid"></div>
                            <h6 class="text-uppercase mt-4 mb-3 fw-bold">
                                Notes
                            </h6>
                            <p class="text-muted mb-0">
                                Terimakasih telah membeli dari sedona.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<style>
    @media print {
        body * {
            visibility: hidden;
        }

        #invoice,
        #invoice * {
            visibility: visible;
        }

    }
</style>
@endpush
@push('js')
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script>
    function pdf() {
        var printContents = document.getElementById("invoice").innerHTML;
		var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
		print();
		document.body.innerHTML = originalContents;
    }

</script>
@endpush
