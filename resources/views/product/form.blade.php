<div class="col-sm-12">
    <div class="form-group" id="name">
        {!! Form::label('name', 'Nama Produk', ['class'=>'placeholder']) !!}
        {!! Form::input('name', 'name', old('email'), ['class' => 'form-control']) !!}
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group" id="price">
        {!! Form::label('price', 'Harga', ['class'=>'placeholder']) !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Rp.</span>
            </div>
            {!! Form::input('text', 'price', old('price'), ['class' => 'form-control',
            'aria-describedby'=>"basic-addon1"]) !!}
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group" id="qty">
        {!! Form::label('qty', 'Jumlah', ['class'=>'placeholder']) !!}
        {!! Form::input('number', 'qty', old('qty'), ['class' => 'form-control']) !!}
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group" id="outlet_id">
        {!! Form::label('outlet_id', 'Outlet', ['class'=>'placeholder']) !!}
        {!! Form::select('outlet_id', $outlets, old('outlet_id'), ['style' => 'width:100%;']) !!}
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group" id="category_id">
        {!! Form::label('category_id', 'Kategori Produk', ['class'=>'placeholder']) !!}
        {!! Form::select('category_id', $categories, old('outlet_id'), ['style' => 'width:100%;']) !!}
    </div>
</div>
