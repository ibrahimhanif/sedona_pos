@extends('layouts.app')
@section('title', 'Pegawai')
@section('content')
    <x-data-table title="List User" :header="$header_table"  resource="users">
        @include('user.form');
    </x-data-table>
@endsection
