<div class="col-lg-4 mt-0">
    <div class="card">
        <div class="card-header">
            Data Kelola Stok
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('description', 'Deskripsi', ['class' => 'placeholder']) !!}
                    {!! Form::textarea('description', NULL, ['class' => 'form-control', 'rows'=>3]) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('type', 'Type Inventory', ['class' => 'placeholder']) !!}
                    {!! Form::select('type', ['in' => 'Stock Masuk', 'out' => 'Stock Keluar'], 'in', ['class' =>
                    'form-control',
                    'style' => 'width: 100%;']) !!}
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('outlet_id', 'Outlet', ['class' => 'placeholder']) !!}
                    <div class="select2-input">
                        {!! Form::select('outlet_id', $outlets, NULL, ['class' => 'form-control', 'style' => 'width:
                        100%;']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-8">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        {!! Form::label('product', 'Poduk ', ['class'=>'placeholder']) !!}
                        <select class="form-control" style="width: 100%;" name="product" id="product">
                            @foreach($products as $product)
                            <option value="{{$product->id}}" data-price="{{$product->price}}"
                                data-qty="{{$product->qty}}">{{$product->name}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('jumlah', 'Jumlah ', ['class'=>'placeholder']) !!}
                        {!! Form::number('jumlah', null, ['class' => 'form-control','max' =>1]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <input type="button" onclick="addbarang()" class="btn btn-primary btn-full" value="Tambah"
                        style="margin-top: 40px;">
                </div>
            </div>

            <table class="table" style="width: 100%" id="table_barang">
                <thead>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

@push('js')
<script>
    var url = "{{route('inventories.store')}}"
    var barang = [];
    // $("#close_btn").click(function(){
    //     barang = [];
    //     refresh_table();
    // });
    function save_trans(){
        let form = new FormData();
        form.append('description', $("#description").val());
        form.append('type', $("#type").val());
        form.append('list_barang', JSON.stringify(barang));
        form.append('outlet_id', $("#outlet_id").val());
        $.ajax({
            url: method == 'POST' ? url : url + '/' + edited_id,
            type: 'POST',
            data: form,
            processData: false,
            contentType: false,
            dataType: "JSON",
            beforeSend: function () {
                if(method != 'POST'){
                    form.append('_method', 'PATCH');
                }
                delete_error();
            },
            success: function (data) {
                $("#modal_form").modal('hide');
                swal("Pesan", "Berhasil", {
						icon : "success",
						buttons: {
							confirm: {
								className : 'btn btn-success'
							}
						},
				});
                reload_table()
                },
            error: function(xhr, error, errorThrown){
                if (xhr.status == 422) {
                    let message = xhr.responseJSON.message;
                    let errors = xhr.responseJSON.errors;
                    swal('Error', 'Data Tidak Valid \nPastikan Data Barang Tidak Kosong', {
                        icon: 'error',
                        buttons: {
                            confirm:{
                                className: 'btn btn-info'
                            }
                        }
                    });
                }
            }
            });
        }
        class Product{
            constructor(id, name, qty)
            {
                this.id = id;
                this.name= name;
                this.qty= qty;
            }
        }
        var tablebarang = $("#table_barang").dataTable({
            data: barang,
            theme: 'bootstrap',
            columns:[
                {title: 'Nama Barang', data:'name'},
                {title: 'Jumlah', data:'qty'},
            ],
            pageLength: 3,
        });
        function refresh_table(){
            tablebarang.fnClearTable();
            barang.forEach(function (val) {
                tablebarang.fnAddData(val);
            });
        }
        function addbarang(){
            let product_id = $("#product").val();
            let product_qty = parseInt($("#jumlah").val());
            if(product_qty < 1 || Number.isNaN(product_qty)){
                swal("Pesan", "Jumlah Barang Tidak Boleh Kurang dari 1", {
                    icon: "warning",
                    buttons:{
                        confirm:{
                            className : 'btn btn-success'
                        }
                    }
                });
            }
            else{
                let name = $('#product').select2('data')[0]['text'];
                let product = new Product(product_id, name, product_qty);
                let item = barang.find(item => item.id == product_id);
                if(item === undefined){
                    barang.push(product);
                    tablebarang.fnAddData(
                        product
                    );
                }else{
                    item.qty = parseInt(item.qty) + parseInt(product_qty);
                    refresh_table();
                }
            }
            let total_harga = barang.map(item => item.final_price)
                                .reduce((prev, curr) => prev + curr, 0);
            $("#final_price").val(total_harga);
        }
</script>
@endpush
