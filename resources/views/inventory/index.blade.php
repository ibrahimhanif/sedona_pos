@extends('layouts.app')
@section('title', 'Kelola Stok')
@section('content')
<x-data-table title="Kelola Stok" :header="$header_table" resource="inventories">
    @include('inventory.form')
</x-data-table>
<div class="modal fade bd-example-modal-lg" id="modal_detail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <table class="table" style="width: 100%" id="table_detail">
                    <thead>
                        <th>Nama Barang</th>
                        <th>Jumlah Perubahan</th>
                        <th>Jumlah Sekarang</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <div class="modal-footer border-0">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="close_btn">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<style>
    #modal_form .modal-dialog {
        width: 100%;
        height: 100%;
        padding: 0;
        margin: 0;
    }

    #modal_form .modal-content {
        height: 100%;
        border-radius: 0;
        color: #333;
        overflow: auto;
    }

    #modal_form .close {
        color: black ! important;
        opacity: 1.0;
    }

    @media (min-width: 992px) {
        #modal_form .modal-lg {
            max-width: 100%;
        }
    }

    @media (min-width: 576px) {
        #modal_form .modal-lg {
            max-width: 100%;
        }
    }
</style>
@endpush
@push('js')

<script>
    var barang = [];
    class Barang{
        constructor(name, jumlah_perubahan, jumlah_sekarang){
            this.name = name;
            this.jumlah_perubahan = jumlah_perubahan;
            this.jumlah_sekarang = jumlah_sekarang;
        }
    }
    var table_detail = $("#table_detail").dataTable({
                    data:barang,
                    theme: 'bootstrap',
                    columns:[
                        {title: 'Nama Barang', data:'name'},
                        {title: 'Jumlah Perubahan', data:'jumlah_perubahan'},
                        {title: 'Jumlah Sekarang', data:'jumlah_sekarang'},
                    ],
                });
    function show_inventory_detail(id){
        let url = "{{route('inventories.index')}}"+'/'+id;
        barang = []
        $.ajax({
            url: url,
            type:'GET',
            dataType: 'JSON',
            success: function(data){
                data.products.forEach(product => {
                    console.log(product);
                    let temp_product = new Barang(product.name, product.pivot.qty, product.qty);
                    barang.push(temp_product);
                });
                table_detail.fnClearTable();
                barang.forEach(function (val) {
                    table_detail.fnAddData(val);
                });
                $("#modal_detail").modal('show');
            }

        })
    }
</script>
@endpush
